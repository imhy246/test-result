import React from 'react';
import './style.css'
import { useForm } from "react-hook-form";
import { v4 as uuidv4 } from 'uuid';

interface Props {
    method: "ADD" | "UPDATE",
    dataUpdate?: Task,
    addTask?: (task: Task) => void;
    updateTask?: (task: Task) => void;
    onDetailTask?: (task: Task | null) => void;
    closeModal?: () => void;
}

const TodoForm = (props: Props) => {
    const { method, addTask, dataUpdate, updateTask, onDetailTask, closeModal } = props;
    const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();

    const onSubmit = (data: any) => {
        if (method === 'ADD') {
            if (addTask) {
                const id = uuidv4();
                addTask({ ...data, id, });
                closeModal && closeModal();
            }
        } else if (method === 'UPDATE' && updateTask && dataUpdate) {
            updateTask({ id: dataUpdate.id, ...data });
            onDetailTask && onDetailTask(null);
        }
        reset();
    }
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className='todo-form'>
                <div>
                    <p>
                        Name
                    </p>
                    <input defaultValue={dataUpdate?.name ?? ''} {...register("name", { required: true })} />
                    {errors.name && <p className='error-message'>Please enter task name!</p>}
                </div>
                <div>
                    <p>
                        Description
                    </p>
                    <textarea defaultValue={dataUpdate?.description ?? ''} {...register("description")} />
                </div>
                <div className='todo-form__option'>
                    <div>
                        <p>Due date</p>
                        <input min={new Date().toISOString().split('T')[0]} defaultValue={method === 'UPDATE' && dataUpdate ? dataUpdate.dueDate : new Date().toISOString().split('T')[0]} {...register("dueDate")} type={'date'} />
                    </div>
                    <div>
                        <p>Priority</p>
                        <select defaultValue={dataUpdate?.priority ?? 'low'} {...register("priority")}>
                            <option value="low">Low</option>
                            <option value="normal">Normal</option>
                            <option value="high"> High</option>
                        </select>
                    </div>
                </div>
                <button type='submit' className='todo-form__button' >{method === 'ADD' ? 'Add' : 'Update'}</button>
            </div>
        </form>
    )
}

export default TodoForm;