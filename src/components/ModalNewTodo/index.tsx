import React from 'react';
import TodoForm from '../TodoForm';
import './style.css';

interface Props {
    show: boolean;
    closeModal: () => void;
    addTask: (task: Task) => void
}

const ModalNewTodo = (props: Props) => {
    const { show } = props;
    return <div className="modal" style={{ display: show ? 'block' : 'none' }}>
        <div className='modal-content'>
            <div className='modal-title'>
                <span>Add task</span>
                <div onClick={() => props.closeModal()}>X</div>
            </div>
            <TodoForm addTask={props.addTask} closeModal={props.closeModal} method='ADD' />
        </div>
    </div>
}

export default ModalNewTodo;