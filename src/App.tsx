import React, { useEffect, useRef, useState } from 'react';
import ModalNewTodo from './components/ModalNewTodo';
import NewTodo from './components/NewTodo';
import TodoForm from './components/TodoForm';
import TodoItem from './components/TodoItem';
import useIsMobile from './hooks';
import { setData } from './utils/storage';
function App() {
  const [showNewTodo, setShowNewTodo] = useState(false);
  const [tasks, setTasks] = useState<Task[]>([]);
  const [detailTask, setDetailTask] = useState<Task | null>(null);
  const [searchTasks, setSearchTasks] = useState<Task[]>([]);
  const [keyword, setKeyword] = useState('');
  const ref = useRef<any>(null);
  const [selectedTasks, setSelectedTasks] = useState<string[]>([])
  const isMobile = useIsMobile();

  useEffect(() => {
    try {
      const data = localStorage.getItem('tasks');
      const tasks = JSON.parse(data as any);
      setTasks(tasks ? tasks : [])
    }
    catch (err) {
      console.log(err)
      setTasks([])
    }

  }, [])

  useEffect(() => {
    localStorage.setItem('tasks', JSON.stringify(tasks))
  }, [tasks])

  const addTask = (task: Task) => {
    setTasks(state => [task, ...state]);
  }

  const updateTask = (task: Task) => {
    setTasks(state => state.map(x => x.id === task.id ? task : x))
  }

  const onDetailTask = (task: Task | null) => {
    setDetailTask(task);
  }

  const removeTask = (id: string) => {
    setTasks(state => state.filter(x => x.id !== id))
  }

  const updateSelectedTask = (id: string, type: boolean) => {
    if (type) {
      setSelectedTasks(state => [...state, id]);
    } else { setSelectedTasks(state => state.filter(x => x !== id)) }
  }

  console.log(showNewTodo)

  return (
    <div className="app container">
      {!isMobile && <div className='app__add-todo'>
        <NewTodo addTask={addTask} />
      </div>}
      <div className='app__todo-list' >
        <div className='app__todo-list-title'>
          <span>To do list</span>
        </div>
        <div className='app__todo-list-search'>
          <input value={keyword} onChange={(e) => {
            if (ref.current) clearTimeout(ref.current);
            setKeyword(e.target.value)
            ref.current = setTimeout(() => {
              setSearchTasks(tasks.filter(x => x.name.includes(e.target.value)))
            }, 200)
          }} placeholder='Search tasks...' />
          {isMobile && <button onClick={() => {
            setShowNewTodo(true);
          }}>Add</button>}
        </div>

        {/* <TodoForm/> */}
        <div className='todo-list'>
          {(keyword ? searchTasks : tasks).map(x => {
            return <div key={x.id}>
              <TodoItem selectedTasks={selectedTasks} updateSelectedTask={updateSelectedTask} removeTask={removeTask} onDetailTask={onDetailTask} data={x} />
              {detailTask?.id === x.id && <div className='todo-list-detail'>
                <TodoForm onDetailTask={onDetailTask} dataUpdate={detailTask} method='UPDATE' updateTask={updateTask} />
              </div>}
            </div>
          })}
        </div>
        {selectedTasks.length > 0 && <div className='bulk-action'>
          <span>Bulk actions</span>
          <div>
            <button onClick={() => { setSelectedTasks([]) }}>
              Done
            </button>
            <button onClick={() => { setSelectedTasks([]); setTasks(state => state.filter(x => !selectedTasks.includes(x.id))) }}>
              Remove
            </button>
          </div>
        </div>}

      </div>
      {isMobile && <ModalNewTodo addTask={addTask} closeModal={() => setShowNewTodo(false)} show={showNewTodo} />}
    </div>

  );
}

export default App;
