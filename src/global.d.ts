interface Task {
    id: string,
    name: string, 
    description: string;
    dueDate: string;
    priority: 'low' | 'normal' | 'high';
}