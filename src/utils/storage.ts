export const getData = (key: string, defaultValue: any) => {
    try {
        const data = localStorage.getItem(key);
        return data ?? defaultValue;
    }
    catch(err) {
        return defaultValue;
    }
}

export const setData = (key: string, value: any) => {
    return localStorage.setItem(key, value);
}

export const removeData = (key: string) => {
    return localStorage.removeItem(key);
}