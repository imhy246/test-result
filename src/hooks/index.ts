import { useEffect, useRef, useState } from "react";

export default function useIsMobile() {
    const [screenSize, setScreenSize] = useState<any>(window.innerWidth);

    const onResize = () => {
        setScreenSize(window.innerWidth)
    }

    useEffect(() => {
        window.addEventListener("resize", onResize);
        return () => {
            window.removeEventListener("resize", onResize)
        }
    }, []);

    return screenSize <= 768;
}