import React from 'react';
import TodoForm from '../TodoForm';
import './style.css';

interface Props {
    addTask: (task: Task) => void
}

const NewTodo = (props: Props) => {
    return <div className='new-todo'>
        <div className='new-todo__header'>
            <span>New Task</span>
        </div>
        <TodoForm addTask={props.addTask} method='ADD' />
    </div>
}

export default NewTodo;