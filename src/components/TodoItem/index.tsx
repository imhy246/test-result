import React from 'react';
import './style.css';

interface Props {
    data: Task;
    onDetailTask: (task: Task) => void;
    removeTask: (id: string) => void;
    updateSelectedTask: (id: string, type: boolean) => void;
    selectedTasks: string[]
}

const TodoItem = (props: Props) => {
    const { data } = props;
    return (
        <div className='todo-item'>
            <div className='todo-name'>
                <input checked={props.selectedTasks.includes(data.id)} onChange={(e) => {
                    props.updateSelectedTask(data.id, e.target.checked)
                }} type="checkbox" />
                <span>{data.name}</span>
            </div>
            <div className='todo-actions'>
                <button onClick={() => { props.onDetailTask(data) }}>Detail</button>
                <button onClick={() => { props.removeTask(data.id) }}>Remove</button>
            </div>
        </div>
    )
}

export default TodoItem;